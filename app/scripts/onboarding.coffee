onboarding = $.noop()
layer = $.noop()

init = ->
  initOnboarding()
  bind()
  onboarding.start()

initOnboarding = ->
  onboarding = new Shepherd.Tour
    defaults:
      classes: 'shepherd-element shepherd-open shepherd-theme-arrows shepherd-element--small shepherd-element--dark'
      showCancelLink: false

  onboarding.addStep 'Dashboard',
    text: [
      '<h1 class="zeta color-success">Dashboard</h1>'
      'Aquí encontrarás un resumen de los últimos Eventos y estadísticas de tu perfil.'
    ]
    attachTo: '.js-onboarding-dashboard'
    buttons: [
      text: 'Next'
      action: onboarding.next
      classes: 'btn--default'
    ]

  onboarding.addStep 'Eventos',
    text: [
      '<h1 class="zeta color-success">Eventos</h1>'
      'Aquí encontrarás todos los Eventos en los que puedes participar.'
    ]
    attachTo: '.js-onboarding-eventos'
    buttons: [
      text: 'Next'
      action: onboarding.next
      classes: 'btn--default'
    ]

  onboarding.addStep 'Premios',
    text: [
      '<h1 class="zeta color-success">Premios</h1>'
      '¡Tú sección favorita! Los mejores premios que <span class="color-success">Bet</span>Box trae para tí.'
    ]
    attachTo: '.js-onboarding-premios'
    buttons: [
      text: 'Next'
      action: onboarding.next
      classes: 'btn--default'
    ]

  onboarding.addStep 'Ranking',
    text: [
      '<h1 class="zeta color-success">Ranking</h1>'
      'Entérate de quien lleva el liderazgo en <span class="color-success">Bet</span>Box en la sección de Ranking.'
    ]
    attachTo: '.js-onboarding-ranking'
    buttons: [
      text: 'Finalizar'
      action: onboarding.complete
      classes: 'btn--default'
    ]


bind = ->
  onboarding.on 'start', startHandler
  onboarding.on 'complete', completeHandler


unbind = ->
  onboarding.off 'start'


startHandler = ->
  addLayer()

completeHandler = ->
  removeLayer()
  unbind()

createLayer = ->
  div = document.createElement 'div'
  div.className = 'shepherd-layer'
  div

addLayer = ->
  layer = createLayer()
  document.body.appendChild layer

removeLayer = ->
  layer.style.opacity = 0
  setTimeout ( ->
      document.body.removeChild(layer)
      return
  ), 300

$ init