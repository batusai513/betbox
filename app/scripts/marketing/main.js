/*global Modernizr */
(function($, Modernizr){
	'use strict';

	$('#tab-container').easytabs({
		tabs: '> .horizontal-container > ul > li',
		tabActiveClass: 'is-active'
	});

})(jQuery, Modernizr);