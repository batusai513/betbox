/*global Modernizr */
(function($, Modernizr){
	'use strict';

	$('.js-events-carousel').on('jcarousel:create jcarousel:reload', function() {
		var element = $(this),
			width = element.innerWidth();

		if (width >= 1200) {
			width = width / 4;
		} else if (width >= 600) {
			width = width / 3;
		} else if (width >= 350) {
			width = width / 2;
		}

        element.jcarousel('items').css('width', width + 'px');
    }).jcarousel({
		transitions: Modernizr.csstransitions ? {
			transforms: Modernizr.csstransforms,
			transforms3d: Modernizr.csstransforms3d,
			easing: 'ease'
		} : false
    });

	$('.jcarousel__arrow.left').jcarouselControl({
		target: '-=1'
	});
	$('.jcarousel__arrow.right').on('jcarouselcontrol:active', function(){
			$(this).removeClass('inactive');
		}).on('jcarouselcontrol:inactive', function(){
			$(this).addClass('inactive');
		}).jcarouselControl({
		target: '+=1'
	});

	$('.jcarousel__arrow.left').on('jcarouselcontrol:active', function(){
			$(this).removeClass('inactive');
		}).on('jcarouselcontrol:inactive', function(){
			$(this).addClass('inactive');
		}).jcarouselControl({
		target: '-=1'
	});

	$('.js-sidebar-btn').on('click', function(e){
		var $el = $(this),
			$parent = $el.parent();
		$el.toggleClass('is-active');
		$parent.toggleClass('is-active');
		e.preventDefault();
	});

	$('#tab-container').easytabs({
		tabs: '> .horizontal-container > ul > li',
		tabActiveClass: 'is-active'
	});

	if ($('.js-jugar').length > 0) {
		$('.js-jugar').magnificPopup({
			type: 'inline',
			items:[
			{
				src: '#confirmacion'
			},
			{
				src: '#invitar'
			}
			]
		});
	};

	var ValidateScore = (function(){
		var firstInput = $.noop(),
			secondInput = $.noop(),
			scoreBoard = $.noop(),
			firstValue = undefined,
			lastValue = undefined,
			canOpen = true;

		function init(options){
			firstInput = options.first;
			secondInput = options.last;
			scoreBoard = options.scoreBoard;
			events();
		}

		function events(){
			$(firstInput).on('keyup', function(e){
				firstValue = $(this).val();
				toggleBoard();
			});

			$(secondInput).on('keyup', function(e){
				lastValue = $(this).val();
				toggleBoard();
			});
		}

		function compare(){
			if(firstValue && lastValue && canOpen){
				if(firstValue === lastValue){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}

		function toggleBoard(){
			scoreBoard.toggle(compare())
		}

		return{
			init: init
		}
	}());

	$('.js-score').on('keydown', function(e){
		return ( event.ctrlKey || event.altKey
                    || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false)
                    || (95<event.keyCode && event.keyCode<106)
                    || (event.keyCode==8) || (event.keyCode==9)
                    || (event.keyCode>34 && event.keyCode<40)
                    || (event.keyCode==46) );
	});

	ValidateScore.init({
		first: $('.js-score').first(),
		last: $('.js-score').last(),
		scoreBoard: $(".scores__tie")
	})

})(jQuery, Modernizr);